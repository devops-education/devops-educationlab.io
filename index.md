---
title: Home
nav_order: 1
description: "DevOps for Education Guide"
permalink: /
---

# DevOps in Education Guide


The DevOps for Education Guide is a resource for educators on how to bring DevOps to your Campus.

The DevOps for Education Guide is created and maintained by the GitLab for Education Team. Our mission is to promote GitLab and DevOps at educational institutions around the world. We aim to build a community of educators, learners, and researchers who are passionate about all things related to DevOps and GitLab.

This guide contains a wide variety of resources on topics including:

- How to apply to our GitLab for Education Program
- How to structure your license and manage students seats
- How to automating your classroom mangement
- Curriculum guides and lessons
- Workshop templates
- Collection of blog posts
- Collection of articles about DevOps in Education

Our wider community and program members are welcome to contribute to our guide. Contributions guidelines are in progress. In the meantime, please open an issue here with your idea for a contribution and we'l work with you directly.

Thanks for reading!

The GitLab for Education Team.
