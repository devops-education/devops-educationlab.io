---
title: Workshops on DevOps in Education
nav_order: 1
parent: Resources for DevOps in Education
---

# Workshops for DevOps in Education

Here you will find workshops for on various topics related to DevOps in Education.


## Workshop list

|Title | Source | Audience | Technology Required | Pace
|
|------|--------|----------|---------------------|
|[Courseware as Code with GitLab](https://devops-education.gitlab.io/cwac-workshop/) | GitLab for Education Team | Faculty and staff who want to automate classroom management with GitLab and use GitLab to generate and manage course content. | GitLab; Docker optional| Self-paced or Instructor-led |
|[Student Contributor Workshop](https://gitlab.com/devops-education/student-contributor-workshop) | GitLab for Education Team | Faculty, staff, or student orgs, interested in learning more about open source and how to contribute to GitLab | GitLab | Self-paced or Instructor-led |
|[Python Twitter Bot with Pj](https://devops-education.gitlab.io/workshops/python-twitter-workshop)| Pj Metz or self led | Student orgs or classes interested in working with an API to build more developer skills. | GitLab, Twitter | 2 hours |
|[Python Magic 8 Ball with Pj](https://gitlab.com/devops-education/workshops/python-8-ball-workshop) | Pj Metz or self led | New programmers looking for a way to learn to use the WEb IDE and practice some python | 1 hour |

## Lectures

Ask us about lectures avaialble for a variety of classes! 

