---
title: Resources for DevOps in Education
nav_order: 3
has_children: true
---

# Resources DevOps in Education

This page hosts resources relevant to bringing DevOps to Education.
