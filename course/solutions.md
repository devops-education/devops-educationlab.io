---
title: Overview of GitLab Solutions for Education
nav_order: 2
has_children: true
---


# Overview of GitLab Solutions for Education

GitLab makes it easy to bring the digital transformation to your campus with the only complete DevOps Platform.

We offer a several licensing and pricing models to Educational Institutions, including free unlimited seats of our top-tier, GitLab Ultimate, through our GitLab for Education Program. 

This section outlines the differences and provides a guide to choosing best option for your campus.
