---
title: GitLab Hosting Options
nav_order: 2
parent: Overview of GitLab Solutions for Education
---

# GitLab Hosting Options at Educational Institutions

GitLab has two hosting options, self-managed and software as a service (Saas). These options are available in any tier and in all of our offerings. The price and the offering is the same regardless of hosting type.  

Feature by feature, GitLab SaaS and self-managed are mostly the same. The key differences are primarily configuration and administration tasks. GitLab self-managed provides the most flexibility for institutions but requires more overhead in installing, hosting, management, backup, recovery, and upgrades. The standout advantages of self-managed commonly cited by GitLab Education Program Members and Customers are the flexibility in user management and the ability to meet external requirements for security and privacy. For example, self-managed GitLab allows institutions to meet requirements when sensitive research cannot be hosted in the cloud.


|Deployment Method| Advantages for Educational Institutions| Considerations|
|---------|--------------------|----------------|
| Self-Managed | Institution controls the hosting environment. |  Technical overhead required. |
|  | Flexibility to deploy on-premises or in the cloud of your choice. | Institution responsible for all instance-level back ups, recovery, and upgrades. |
|   | Institution can apply custom settings for the instance. | Institutions incurs hosting costs. |
|     | Institution is the admin of the instance. |  |
|     | Unrestricted access to log information and auditing |   |
|     | Ability to control user sign up experience with campus sign on system |   |
|    | Ability control terms of service experience at sign up |  |
|    | Ability to meet limitations for sensitive research data that cannot be stored in the cloud |  
|SaaS | Low overhead for IT to download, install, manage  | Institution does not have control over the Architecture, and instance-level backups, recovery, and upgrades |
|  | No technical set up required | Custom instance settings for users are not available.  |
| | Instance wide settings for all users | All users, including students, must create accounts on GitLab.com |
|  | Institution is the group owner |   |
|  | SAML SSO is available. |  |

[More details here](https://about.gitlab.com/handbook/marketing/strategic-marketing/dot-com-vs-self-managed/) on the key differences and a full list of all differences can be found [here](https://about.gitlab.com/handbook/marketing/strategic-marketing/dot-com-vs-self-managed/). 
