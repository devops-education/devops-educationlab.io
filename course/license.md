---
title: GitLab Licensing Options
nav_order: 1
parent: Overview of GitLab Solutions for Education
---

# GitLab Licensing Options at Educational Institutions

There are several different ways that educational institutions can obtain a license for GitLab at their Educational Institution.

|Offering | Cost | Restrictions | Advantages for Educational Institutions|
|---------|------|--------------|----------------------------------------|
| Free | $0 per user/per month | No credit card required to sign up.|-  All use cases in one license |
| | | Credit/debit card is required to run CI/CD pipelines. Credit/debit card is only used for verification purposes.| - No need for administrative overhead in managing use cases |
| | |5GB storage. | - Features span the DevOps lifecycle |
| | | 10GB data transfer per month. | - Open core is extensible for building custom tools |
| | | 400 CI/CD minutes per month. | - Ability to bring your own GitLab CI runners |
| | | [Different limitations exist for SaaS and Self Hosted](https://about.gitlab.com/pricing/)
| [GitLab for Education](https://about.gitlab.com/solutions/education/) | $0 per user/per month; Unlimited users per institution | Available only for the purposes of teaching, learning, and research directly. It is not permitted to run or operate an institution with free seats. (see program requirements)| - Ultimate, GitLab’s top tier for free in self-managed (campus servers) or Software as a Service (GitLab's cloud). |
| | | | Great for classroom use |
| | | | Great for research lab use |
| | | | Great for use at the department or college level|
| GitLab Standard Educational Discount | Standard 20% discount on any tier; Price per user depends on the tier | No use case limitations | - Pay only for the seats you need. |
| | | | - Great for institutions with small team of developers |
| | | | - Can be used in combination with GitLab for Education free licenses. Separate licenses required. |
| [GitLab for Campuses](https://about.gitlab.com/solutions/education/campus/) | Price based on the size of the institution; not on the number of users | No use case limitations for all users within the institution’s email domain. The license extends to sub-units of the institution such as research institutes.  The license does not extend above to parent units if purchased at a subunit level. | - Great for campuses wide  and/or growing adoption.|
| | | | - Great for campuses that want to collaborate across the institution regardless of use case.|
| | | | - Great for campuses that don’t want to worry about the number of users. |
| | | | - Great for campuses that want a predictable low cost that accounts for growth. |


See features by [tier](https://about.gitlab.com/pricing/).
